import 'package:flutter/material.dart';

void main() => runApp(MyApp()); // 1 line func

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = new Container(
      child: new Row(
        children: <Widget>[
          new Expanded(child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: new Text("League of Legends skin channel", style: new TextStyle(fontWeight: FontWeight.bold))
              ),
              new Text("This channel contains collection of LoL skin images", style: new TextStyle(color: Colors.grey[850], fontSize: 16.0)),
            ],
          )),
          new Icon(Icons.favorite, color: Colors.red),
          new Text("100", style : new TextStyle(fontSize: 18.0)),
        ]
      )
    );
    Widget buildButton(IconData icon, String buttonTitle) {
      final Color tintColor = Colors.blue;
      return new Column(
        children: <Widget>[
          new Icon(icon, color: tintColor),
          new Container(
            margin: const EdgeInsets.only(top: 5.0),
            child: new Text(buttonTitle, style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600)),
          )
        ]
      );
    }
    Widget fourButtonsSection = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          buildButton(Icons.home, "Home"),
          buildButton(Icons.arrow_back, "Back"),
          buildButton(Icons.arrow_forward, "Next"),
          buildButton(Icons.share, "Share"),
      ],),
    );
    final bottomTextSection = new Container(
      padding: const EdgeInsets.all(20.0),
      child: new Container(
        child: new Text('''I am Truong sieu cap vip pro\nI am Truong sieu cap vip pro\nI am Truong sieu cap vip pro'''),
      ),
    );
    return new MaterialApp(
      title: "This is my first flutter app",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Flutter App'),
        ),
        body: new ListView(
          children: <Widget>[
            new Image.asset('images/morder.png',
            fit: BoxFit.cover
            ),
            titleSection,
            fourButtonsSection,
            bottomTextSection
          ],
        )
      )
    );
  }
}