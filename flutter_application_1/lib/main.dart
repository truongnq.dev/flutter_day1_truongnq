import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp()); // 1 line func

class RandomEnglistWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new RandomEnglishWordsState();
  }
}

class RandomEnglishWordsState extends State<RandomEnglistWords> {
  final _words = <WordPair>[];
  final _checkedWords = new Set<WordPair>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("List of English Words"),
        actions: <Widget>[
          new IconButton(onPressed: _pushToSavedWordsScreen, icon: new Icon(Icons.list))
        ]
      ),
      body: new ListView.builder(
        itemBuilder: (BuildContext context, int index) {
        if(index >= _words.length) {
          _words.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_words[index], index);
      })
    );
  }

  _pushToSavedWordsScreen() {
    final pageRoute = new MaterialPageRoute(builder: (context){
      final listTiles = _checkedWords.map((wordPair) {
        return new ListTile( 
          title: new Text(wordPair.asUpperCase,
          style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
        );
      });

      return new Scaffold(
        appBar: new AppBar(
          title: new Text("Checked words")
        ),
        body: new ListView(children: listTiles.toList())
      );
    });
    Navigator.of(context).push(pageRoute);
  }

  Widget _buildRow(WordPair wordPair, int index) {
    final color = index % 2 == 0 ? Colors.red : Colors.blue;
    final isChecked = _checkedWords.contains(wordPair);
    return new ListTile(
      leading: new Icon(isChecked ? Icons.check_box : Icons.check_box_outline_blank),
      title: new Text(wordPair.asUpperCase,
      style: new TextStyle(fontSize: 18.0, color: color)),
      onTap: () {
        setState(() {
          if(isChecked) {
            _checkedWords.remove(wordPair);
          } else {
            _checkedWords.add(wordPair);
          }
        });
      },
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "This is my first flutter app",
      home: new RandomEnglistWords()
    );
  }
}
