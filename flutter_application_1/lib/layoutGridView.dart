import 'package:flutter/material.dart';

void main() => runApp(MyApp()); // 1 line func

class MainPage extends StatefulWidget {
  final String title;
  MainPage({this.title = " "}): super();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title)
      ),
      body: new GridView.extent(
        maxCrossAxisExtent: 240.0,
        children: _buildGridTiles(2),
      ),
      
    );
  }
}

List<Widget> _buildGridTiles(numberOfTiles) {
  List<Container> containers = new List<Container>.generate(numberOfTiles, (int index) {
    final imageName = index < 9 ?
                'images/image0${index+1}.png' : 'images/image${index+1}.png';
    return new Container(
      child: new Image.asset(imageName),
    );
  });

  return containers;
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return new MaterialApp(
      title: "This is my first flutter app",
      home: new MainPage(title: "GridView of Images")
    );
  }
}